import pygame


def clear(window):
    window.fill((0, 0, 0))


def text_to_screen(screen, text, generation, x, y, size=50, color=(255, 255, 255), font_type='Sans serif'):
    clear(screen)
    try:
        text = str(text)
        font = pygame.font.SysFont(font_type, size)
        text = font.render(text, True, color)
        screen.blit(text, (x, y))
        text = font.render(str(generation), True, color)
        screen.blit(text, (x, y+50))

    except Exception as e:
        print('Font Error, saw it coming')
        raise e
