import pygame
from pygame.locals import *
import time
from renderer import text_to_screen
from functions import *


def simulate():
    WINDOW_SIZE = (600, 600)

    pygame.init()

    pygame.font.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    goal_sentence = "Genetic Algorithm"

    child_count = 1000
    mutation_rate = 0.1
    best_score_rate = 0.001

    sentences = generate_init_children(goal_sentence, child_count)
    generation = 0
    generation_progress = True

    pause = False

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("Genetic Algorithm")

    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause

        if not pause:
            if frameCount % 1 == 0:
                best, sentences = generate_children(sentences, mutation_rate, best_score_rate, goal_sentence)
                text_to_screen(window, best, generation, 50, 50)
                if best == goal_sentence:
                    generation_progress = False
                if generation_progress:
                    generation += 1

            pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()
