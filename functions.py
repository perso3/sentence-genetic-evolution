import random
import string


def score_children(children, goal_sentence):
    indexes = {}
    for i, child in enumerate(children):
        child_score = 0
        for char, goal_char in zip(child, goal_sentence):
            if char == goal_char:
                child_score += 1

        indexes[i] = child_score

    indexes = sorted(indexes, key=indexes.get, reverse=True)
    scores = []
    for i in indexes:
        scores.append(children[i])

    return scores


def generate_init_children(goal_sentence, count):
    size = len(goal_sentence)
    children = []
    for _ in range(count):
        sentence = ""
        for _ in range(size):
            sentence += random.choice(string.printable)
        children.append(sentence)
    return children


def generate_children(parents, mutation_rate, best_score_rate, goal_sentence):
    scores = score_children(parents, goal_sentence)
    scores_count = len(scores)
    best_children = scores[:int(scores_count*best_score_rate)]
    best = scores[0]
    new_children = []

    for i in range(len(parents)):
        sentence = ""
        a = random.choice(best_children)
        b = random.choice(best_children)

        count = 0
        for a_char, b_char in zip(a, b):
            mutate = random.randrange(0, 100) / 100

            if mutate == mutation_rate:
                sentence += random.choice(string.printable)
            else:
                if count % 2 == 0:
                    sentence += a_char
                else:
                    sentence += b_char

        new_children.append(sentence)

    return best, new_children
